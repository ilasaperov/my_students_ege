"""
19 задание
x – 1 куча
p – чей ход
"""


def f(x, y, p):
    if x + y >= 151 and (p == 2 or p == 4):  # Выигрывает Петька ВТОРЫМ ХОДОМ
        return True
    if x + y < 151 and p == 4:  # Петька идет нахуй вторым ходом
        return False
    if x + y >= 151:  # Либо Ваня выиграл первым ходом, либо Петя
        return False
    if p % 2 == 1:
        return f(x + 1, y, p + 1) or f(x * 4, y, p + 1) or f(x, y + 1, p + 1) or f(x, y * 4, p + 1)
    else:
        return f(x + 1, y, p + 1) and f(x, y + 1, p + 1) and f(x * 4, y, p + 1) and f(x, y * 4, p + 1)


for i in range(1, 141):  # Диапазон S
    if f(9, i, 0):  # 0 - ничей ход, 1 – Петя, 2 – Ваня, 3 – Петя и тд
        print(i)