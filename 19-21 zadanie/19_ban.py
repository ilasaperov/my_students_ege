def f(x, turn, ban):
    if x >= 50 and turn == 2:
        return True
    if x < 50 and turn == 2:
        return False
    if x >= 50:
        return False
    if ban == 1:
        return f(x + 2, turn + 1, 2) or f(x * 2, turn + 1, 3)  # здесь какие ходы могут быть
    elif ban == 2:
        return f(x + 1, turn + 1, 1) or f(x * 2, turn + 1, 3)  # здесь какие ходы могут быть
    elif ban == 3:
        return f(x + 1, turn + 1, 1) or f(x + 2, turn + 1, 2)  # здесь какие ходы могут быть
    elif ban is None:
        return f(x + 1, turn + 1, 1) and f(x + 2, turn + 1, 2) and f(x * 2, turn + 1, 3)  # здесь какие ходы могут быть


for i in range(1, 50):  # Диапазон S
    if f(i, 0, None):  # 0 - ничей ход, 1 – Петя, 2 – Ваня, 3 – Петя и тд
        print(i)