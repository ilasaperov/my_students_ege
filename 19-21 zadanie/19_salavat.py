'''
19 задание
x – 1 куча
y – 2 куча
p – чей ход
'''

def f(x, y, p):
    if x + y >= 41 and p == 2:
        return True
    if x + y < 41 and p == 2:
        return False
    else:
        return f(x + 1, y + 2, p + 1) or f(x + 2, y + 1, p + 1) or f(x * 2, y, p + 1) or f(x, y * 2, p + 1)# здесь какие ходы могут быть

for i in range(1, 33): # Диапазон S
    if f(8, i, 0): # 0 - ничей ход, 1 – Петя, 2 – Ваня, 3 – Петя и тд
        print(i)

