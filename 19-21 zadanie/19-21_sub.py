from functools import lru_cache
from math import ceil


def moves(h):
    a, b = h
    if a > 0 and b > 0:
        return (a - 1, b), (ceil(a / 2), b), (a, b - 1), (a, ceil(b / 2))


@lru_cache(None)
def game(h):
    a, b = h
    if sum(h) <= 40 or a == 0 or b == 0:  # для заданий на 2 кучи, тут делаем ''sum(h)''
        return "w"
    if any(game(m) == "w" for m in moves(h)):
        return "p1"
    if all(game(m) == "p1" for m in moves(h)):  # для 19 задания тут меняем all на any
        return "v1"
    if any(game(m) == "v1" for m in moves(h)):
        return "p2"
    if all(game(m) == "p1" or game(m) == "p2" for m in moves(h)):
        return "v2"


for x in range(21, 100):
    if game((20, x)) is not None:
        print(x, game((20, x)))
