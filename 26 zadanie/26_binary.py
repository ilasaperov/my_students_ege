f = open("inf_26_04_21_26 (2).txt")

a = f.read().splitlines()
a = a[1:]
a = list(map(int, a))
a.sort()

count = 0
max_sum = -1


def binary(array, elem, start, stop):
    if start > stop:
        return False

    mid = (start + stop) // 2

    if elem == array[mid]:
        return True

    if elem < array[mid]:
        return binary(array, elem, 0, mid - 1)
    else:
        return binary(array, elem, mid + 1, stop)


for i in range(len(a) - 1):
    print(i)
    for j in range(i + 1, len(a)):
        if a[i] % 2 != a[j] % 2:
            sum = a[i] + a[j]
            if binary(a, sum, 0, len(a) - 1):
                count += 1
                if sum > max_sum:
                    max_sum = sum

print(count, max_sum)
