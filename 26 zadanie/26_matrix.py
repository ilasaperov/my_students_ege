f = open("26-72.txt")
a = f.read().splitlines()
n, m, k = map(int, a[0].split())
a = a[1:]

count = 0
matrix = [[0 for i in range(n)] for j in range(m)]

for x in a:
    row, number = map(int, x.split())
    matrix[row - 1][number - 1] = 1

max_row_count = 0
max_row = 0
for i in range(m):
    row_count = 0
    for j in range(n - 3):
        if matrix[i][j] == 0 and matrix[i][j + 1] == 0 and matrix[i][j + 2] == 0 and matrix[i][j + 3] == 0:
            count += 1
            row_count += 1
            if row_count > max_row_count:
                max_row_count = row_count
                max_row = i + 1

print(count, max_row)
