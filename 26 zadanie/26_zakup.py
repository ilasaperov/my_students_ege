f = open("26 (22).txt")
a = f.read().splitlines()
money = int(a[0].split()[1])
a = a[1:]

max_a = 0
money_left = money
table = []
for x in a:
    price, type = x.split()
    table.append([int(price), type])

table.sort()
table_b = []
table_a = []

count = 0
count_a = 0
for x in table:
    if money_left - x[0] > 0:
        if x[1] == "A":
            count_a += 1
            table = table[1:]
        else:
            table_b.append(x)
            table = table[1:]
        count += 1
        money_left -= x[0]
    else:
        if x[1] == "A":
            table_a.append(x)

table_b.sort(reverse=True)
print(count, count_a, money_left)
print(table_a)
print(table_b)

for x in table_b:
    if money_left + x[0] - table_a[0][0] > 0:
        count_a += 1
        money_left += x[0] - table_a[0][0]
        table_a = table_a[1:]

print(count_a, money_left)