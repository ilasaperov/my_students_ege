"""
https://inf-ege.sdamgia.ru/problem?id=35915
"""

f = open("26 (40).txt")

a = f.read().splitlines()
a = a[1:]
a = list(map(int, a))
a.sort()

count = 0
max_sr = -1

for i in range(len(a) - 1):
    print(i)
    for j in range(i + 1, len(a)):
        if a[i] % 2 != 0 and a[j] % 2 != 0:
            sr = (a[i] + a[j]) // 2
            if sr in a:
                count += 1
                if sr > max_sr:
                    max_sr = sr

print(count, max_sr)