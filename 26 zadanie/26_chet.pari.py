def binary_search_recursive(array, element, start, end):
    if start > end:
        return False

    mid = (start + end) // 2
    if element == array[mid]:
        return True

    if element < array[mid]:
        return binary_search_recursive(array, element, start, mid - 1)
    else:
        return binary_search_recursive(array, element, mid + 1, end)

f = open("26 (41).txt")
a = f.read().splitlines()
a = a[1:]
a = list(map(int, a))
a.sort()

count = 0
sr_max = 0


for i in range(len(a) - 1):
    for j in range(i + 1, len(a)):
        if a[i] % 2 == 0 and a[j] % 2 == 0:
            sr = (a[i] + a[j]) // 2
            if binary_search_recursive(a, sr, 0, len(a)):
                count += 1
                if sr > sr_max:
                    sr_max = sr
print(count, sr_max)