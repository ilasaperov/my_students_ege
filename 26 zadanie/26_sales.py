from math import ceil

f = open("adammm.txt")

a = f.read().splitlines()
a = list(map(int, a))
a = a[1:]
a.sort()


s = 0
sale = 0
max_sale = 0

last = 0
for i in range(0, len(a)):
    if a[i] <= 100:
        s += a[i]
        last = i

a = a[last + 1:]

flag = False
while len(a) != 0:
    if not flag:
        s += a.pop()
        flag = True
    else:
        k = a.pop(0)
        max_sale = k
        sale += k
        flag = False

sale = ceil(sale * 0.70)

s += sale

print(s, max_sale)