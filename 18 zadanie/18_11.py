f = open("1.txt")
a = f.read().splitlines()
a = list(map(int, a))

count = 0

for i in range(len(a) - 11):
    for j in range(i + 11, len(a)):
        if a[i] + a[j] < 200:
            count += 1

print(count)
