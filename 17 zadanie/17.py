f = open("17-1.txt")
a = f.read().splitlines()
a = [int(x) for x in a]

count = 0
max_sum = -10000000

sr = sum(a) // len(a)

for i in range(len(a) - 2):
    first = [x for x in a[i: i + 3] if x < sr]
    second = [x for x in a[i: i + 3] if abs(x) % 10 == 8]
    if len(first) >= 2 and len(second) >= 2:
        count += 1
        if (a[i] + a[i + 1] + a[i + 2]) > max_sum:
            max_sum = a[i] + a[i + 1] + a[i + 2]

print(count, max_sum)