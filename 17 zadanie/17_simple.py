def f(x):
    count = 0
    for i in range(1, x + 1):
        if x % i == 0:
            count += 1
            if count > 17:
                return True


count = 0
min_number = 100000000
for x in range(30001, 70001):
    if f(x):
        count += 1
        if x < min_number:
            min_number = x

print(count, min_number)