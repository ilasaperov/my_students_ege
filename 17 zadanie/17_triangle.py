def count_one(x):
    s = bin(x)[2:]
    count = s.count("1")
    return count > 5 and count % 2 == 1


f = open("../course/17-6.txt")
a = f.read().splitlines()
a = list(map(int, a))


count = 0
max_sum = 0


for i in range(len(a) - 2):
    if count_one(a[i]) == 3 and count_one(a[i + 1]) == 3 and count_one(a[i + 2]) == 3:
        count += 1
        max_sum += max(a[i], a[i + 1], a[i + 2])
print(count, max_sum)