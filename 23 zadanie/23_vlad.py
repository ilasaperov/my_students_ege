"""
3 · 2164 + 2 · 366 − 648
"""
for s in range(10000):
    x = 216**5 + 6**3 - 1 - s

    count = 0
    while x > 0:
        if x % 6 == 5:
            count += 1
        x //= 6
    if count == 12:
        print(s)
        break

