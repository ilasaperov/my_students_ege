import itertools

s = "полина"

count = 0

for y in itertools.product(s, repeat=4):
    x = ''.join(y)
    if x.count("оо") == 0 and x.count("ои") == 0 and x.count("оа") == 0 and x.count("ио") == 0 and x.count("ии") == 0 and x.count("иа") == 0 and x.count("ао") == 0 and x.count("аи") == 0 and x.count("аа") == 0 and x.count("пп") == 0 and x.count("пл") == 0 and x.count("пн") == 0 and x.count("лп") == 0 and x.count("лл") == 0 and x.count("лн") == 0 and x.count("нп") == 0 and x.count("нл") == 0 and x.count("нн") == 0:
        count += 1

print(count)