f = open()

a = f.read().splitlines()

a = a[1:]
s = 0
min_dif = 1000000000

for i in range(len(a)):
    x, y = map(int, a[i].split())
    s += max(x, y)
    d = abs(x - y)
    if d % 3 != 0 and d < min_dif:
        min_dif = d
if s % 3 != 0:
    print(s)
else:
    print(s - min_dif)
