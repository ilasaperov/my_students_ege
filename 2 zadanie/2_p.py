def f(x):
    a = [int(i) for i in str(x)]
    a.sort()
    max_num = int(str(a[2]) + str(a[1]))
    min_num = int(str(a[0]) + str(a[1]))
    if max_num >= 10 and min_num >= 10:
        return max_num - min_num


for x in range(100, 10000):
    if f(x) == 60:
        print(x)
        break