def f(n):
    s = bin(n)[2:]
    for i in range(3):
        if s.count("1") == s.count("0"):
            s += s[len(s) - 1]
        elif s.count("1") < s.count("0"):
            s += "1"
        else:
            s += "0"
    return int(s, 2)


for n in range(105, 200):
    if f(n) % 4 == 0:
        print(n, f(n))
        break
