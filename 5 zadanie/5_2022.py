def f(x):
    s = bin(x)[2:]
    if s.count("1") % 2 == 0:
        s = "10" + s[2:] + "0"
    else:
        s = "11" + s[2:] + "1"
    return int(s, 2)


for n in range(1000):
    if f(n) > 500:
        print(n)
        break