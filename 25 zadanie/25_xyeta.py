from math import sqrt


def f(x):
    if x % 2 == 0:
        x //= 2
        if sqrt(x) == int(sqrt(x)) and simple(int(sqrt(x))):
            return True
        else:
            return False
    else:
        return False


def simple(x):
    for i in range(2, x):
        if x % i == 0:
            return False
    return True


for x in range(101000000, 102000001):
    if f(x):
        print(x)
