def f(x):
    m = 0
    n = 0
    while x % 2 == 0:
        x //= 2
        m += 1
    while x % 3 == 0:
        x //= 3
        n += 1
    if x == 1 and m % 2 == 0 and n % 2 == 1:
        return True


for x in range(200000000, 400000001):
    if f(x):
        print(x)