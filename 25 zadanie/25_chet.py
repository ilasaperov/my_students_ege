def count(x):
    div = []
    for i in range(2, x + 1):
        if x % i == 0 and i % 2 == 0:
            div.append(i)
    if len(div) == 4:
        return div


for x in range(110203, 110245 + 1):
    if count(x):
        print(count(x))
