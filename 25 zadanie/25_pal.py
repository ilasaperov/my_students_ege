def f(x):
    s = 0
    max_del = 0
    for i in range(2, x):
        if x % i == 0:
            s += i
            max_del = i
    reverse = str(s)[::-1]
    if reverse == str(s):
        return max_del


for x in range(520001, 100000000):
    if f(x):
        print(x, f(x))
