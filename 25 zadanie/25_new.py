def f(x):
    count_2 = 0
    count_3 = 0
    while x % 2 == 0:
        x = x // 2
        count_2 += 1
    while x % 3 == 0:
        x = x // 3
        count_3 += 1
    if count_3 % 2 == 1 and count_2 % 2 == 0 and x == 1:
        return True
    else:
        return False


for x in range(200000000, 400000001):
    if f(x):
        print(x)