from math import sqrt


def f(x):
    s = 0
    count = 0
    for i in range(x - 1, int(sqrt(x) - 1), -1):
        if x % i == 0:
            s += i
            count += 1
        if count == 2:
            break
    if count < 2:
        s = 0
    if s % 31 == 0 and s < 100_000:
        return s


for x in range(10_000_000, 1_000_000_000):
    print(x)
    if f(x):
        print(x)
