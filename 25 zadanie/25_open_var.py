"""
25 с открытого варианта ФИПИ
"""
def f(x):
    min_del = x + 1
    max_del = 0
    min_flag = False
    max_flag = False
    for i in range(2, x):
        if x % i == 0:
            if i < min_del:
                min_del = i
                min_flag = True
            if i > max_del:
                max_del = i
                max_flag = True
    m = min_del + max_del
    if m % 7 == 3 and max_flag and min_flag:
        return m
    else:
        return None


x = 452021
count = 0

while count != 5:
    if f(x) is not None:
        print(x, f(x))
        count += 1
    x += 1
