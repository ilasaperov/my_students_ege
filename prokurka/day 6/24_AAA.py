f = open("24 (32).txt")
a = f.read()
a = a.split("A")
a = list(map(len, a))

max_len = -1

for i in range(len(a) - 1):
    if (a[i] + a[i + 1] + 1) > max_len:
        max_len = a[i] + a[i + 1] + 1
print(max_len)