f = open("24_demo.txt")
a = f.read()

count = 1
max_count = -1

for i in range(len(a) - 1):
    if a[i] != a[i + 1]:
        count += 1
        if count > max_count:
            max_count = count
    else:
        count = 1

print(max_count)