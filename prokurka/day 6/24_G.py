import collections

f = open("24 (28).txt")
a = f.read().splitlines()

min_str = ""
min_G = 1000000000

for x in a:
    if x.count("G") < min_G:
        min_str = x
        min_G = x.count("G")

print(collections.Counter(min_str))
