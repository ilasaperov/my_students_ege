import itertools
s = "борис"

count = 0
for x in itertools.product(s, repeat=6):
    a = ''.join(x)
    if a.count("р") == 1 and a.count("б") == 1 and a.count("с") <= 1:
        count += 1

print(count)