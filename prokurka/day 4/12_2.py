min_len = 0
min_count = 10000000
for i in range(201, 1000):
    s = i * "1"
    while "1111" in s:
        s = s.replace("1111", "22", 1)
        s = s.replace("222", "1", 1)
    if s.count("1") < min_count:
        min_count = s.count("1")
        min_len = i
print(min_count, min_len)
