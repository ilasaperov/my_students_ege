def f(x):
    s = bin(x)[2:]
    s += str(s.count("1") % 2)
    s += str(s.count("1") % 2)
    return int(s, 2)

max_num = -1
for n in range(1000):
    if max_num < f(n) < 100:
        max_num = f(n)
print(max_num)