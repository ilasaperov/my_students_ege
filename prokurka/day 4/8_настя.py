import itertools


def f(s):
    sogl = ["н", "с", "т"]
    gl = ["а", "я"]
    for i in range(len(s) - 1):
        if (s[i] in gl and s[i + 1] in gl) or (s[i] in sogl and s[i + 1] in sogl):
            return False
    return True


s = "настя"
count = 0
for x in itertools.permutations(s, r=4):
    a = ''.join(x)
    if f(a):
        count += 1

print(count)