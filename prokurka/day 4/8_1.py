import itertools

s = "регина"

count = 0
for x in itertools.product(s, repeat=5):
    a = ''.join(x)
    print(a)
    if a.count("р") == 1 and a.count("г") == 1 and a.count("н") <= 1:
        count += 1

print(count)