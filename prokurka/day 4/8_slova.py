import itertools

s = sorted("лемур")

index = 1
for x in itertools.product(s, repeat=4):
    a = ''.join(x)
    if a[0] == "л":
        print(index, a)
    index += 1
