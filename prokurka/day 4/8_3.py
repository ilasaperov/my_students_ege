import itertools

s = "демьян"

count = 0
for x in itertools.permutations(s):
    a = ''.join(x)
    if a[0] != "ь" and "еь" not in a and "яь" not in a:
        count += 1

print(count)