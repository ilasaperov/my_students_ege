f = open()
a = f.read().splitlines()
a = list(map(int, a))

count = 0
max_sum = 0

for i in range(len(a) - 1):
    for j in range(i + 1, len(a)):
        dif = abs(a[i] - a[j])
        sum = a[i] + a[j]
        if dif % 2 == 0 and (a[i] % 31 == 0 or a[j] % 31 == 0):
            count += 1
            if sum > max_sum:
                max_sum = sum

print(count, max_sum)



