"""
 p = 0
 1 - Петя
 2 - Ваня
"""


def f(x, y, p):
    if x + y >= 79 and p == 2:
        return True
    if x + y < 79 and p == 2:
        return False
    return f(x + 1, y, p + 1) or f(x, y + 1, p + 1) or f(x * 3, y, p + 1) or f(x, y * 3, p + 1)


for s in range(1, 73):
    if f(6, s, 0):
        print(s)
