f = open("27_A (9).txt")
a = f.read().splitlines()[1:]
a = list(map(int, a))

max_sum = -1
min_len = 10000000000

for i in range(len(a)):
    s = 0
    l = 0
    for j in range(i, len(a)):
        s += a[j]
        l += 1
        if s % 43 == 0:
            if s > max_sum:
                max_sum = s
                min_len = l
            elif s == max_sum and l < min_len:
                min_len = l

print(max_sum, min_len)