f = open("1234.txt")
a = f.read().splitlines()[:-1]
a = list(map(int, a))

max_7 = -1
m = -1

for x in a:
    if x % 7 == 0 and x % 49 != 0 and x > max_7:
        max_7 = x
    elif x % 7 != 0 and x > m:
        m = x

print(m * max_7)