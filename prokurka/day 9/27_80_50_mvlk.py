f = open("28130_B (1).txt")
a = f.read().splitlines()[1:]
a = list(map(int, a))

count = 0

for i in range(len(a) - 1):
    for j in range(i + 1, len(a)):
        s = a[i] + a[j]
        if s % 80 == 0 and (a[i] > 50 or a[j] > 50):
            count += 1

print(count)