f = open("27986_A.txt")
a = f.read().splitlines()[:-1]
a = list(map(int, a))

max_prod = -1

for i in range(len(a) - 1):
    for j in range(i + 1, len(a)):
        prod = a[i] * a[j]
        if prod % 7 == 0 and prod % 49 != 0 and prod > max_prod:
            max_prod = prod

print(max_prod)