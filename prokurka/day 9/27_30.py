

f = open("27-B (12).txt")
a = f.read().splitlines()[1:]
a = list(map(int, a))

max_sum = -1
sum = 0
mins = [2*10**9 + 1 for i in range(30)]
k = 0
for x in a:
    sum += x
    if x % 2 == 1 and x > 0:
        k += 1
    ost = k % 30
    if ost == 0:
        max_sum = max(max_sum, sum)

    if sum < mins[ost]:
        mins[ost] = sum
    else:
        if sum - mins[ost] > max_sum:
            max_sum = sum - mins[ost]
            b = [sum, mins[ost]]

