a = "BABABABBBBBBBBBBBBOBOBOBOBOBOBOBO"

a = a.replace("BA", "*")
a = a.replace("BO", "*")
a = a.replace("DA", "*")
a = a.replace("DO", "*")
a = a.replace("CA", "*")
a = a.replace("CO", "*")

count = 0
max_count = -1

for x in a:
    if x == "*":
        count += 1
        max_count = max(count, max_count)
    else:
        count = 0

print(max_count)