f = open("28131_A.txt")
a = f.read().splitlines()[1:]
a = list(map(int, a))

first = 0
second = 0
max_sum = -1

for i in range(len(a) - 1):
    for j in range(i + 1, len(a)):
        sum = a[i] + a[j]
        if a[i] > a[j] and sum > max_sum and sum % 120 == 0:
            max_sum = sum
            first = a[i]
            second = a[j]

print(first, second)