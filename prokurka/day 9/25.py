import re

for x in range(1, 10**8):
    s = str(x)
    if re.fullmatch("123\d*67", s):
        if x % 123 == 0:
            print(x, x // 123)