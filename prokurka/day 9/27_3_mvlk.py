f = open("28128_A.txt")
a = f.read().splitlines()[1:]
a = list(map(int, a))

max_sum = -1

for i in range(len(a) - 1):
    for j in range(i + 1, len(a)):
        s = a[i] + a[j]
        if s > max_sum and s % 3 == 0:
            max_sum = s

print(max_sum)