f = open("27-B (9).txt")
a = f.read().splitlines()[1:]

s1 = s2 = s3 = 0
min_dif = 100000000
for x in a:
    k, l, m = map(int, x.split())
    mn = min(k, l, m)
    mx = max(k, l, m)
    sr = k + l + m - max(k, l, m) - min(k, l, m)
    s3 += mn
    s2 += mx
    s1 += sr
    if abs(sr - mn) < min_dif and abs(sr - mn) % 2 != 0:
        min_dif = abs(sr - mn)

print(s1 % 2, s2 % 2, s3, min_dif)