f = open("28128_B.txt")
a = f.read().splitlines()[1:]
a = list(map(int, a))

max_0 = []
max_1 = -1
max_2 = -1

for x in a:
    if x % 3 == 0:
        max_0.append(x)
    elif x % 3 == 1 and x > max_1:
        max_1 = x
    elif x % 3 == 2 and x > max_2:
        max_2 = x

max_0.sort(reverse=True)

print(max_0)
print(max_1)
print(max_2)
