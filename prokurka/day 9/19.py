def f(x, p):
    if x >= 129 and p == 2:
        return True
    if x < 129 and p == 2:
        return False
    if x >= 129:
        return False
    if p % 2 == 0:
        return f(x + 1, p + 1) and f(x * 2, p + 1)
    else:
        return f(x + 1, p + 1) or f(x * 2, p + 1)


for s in range(1, 129):
    if f(s, 0):
        print(s)