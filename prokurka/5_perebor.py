def f(x):
    if x % 2 == 0:
        x //= 2
    else:
        x -= 1
    if x % 3 == 0:
        x //= 3
    else:
        x -= 1
    if x % 5 == 0:
        x //= 5
    else:
        x -= 1
    return x


count = 0
for x in range(2, 100000):
    if f(x) == 1:
        count += 1

print(count)