def simple(x):
    for i in range(2, x):
        if x % i == 0:
            return False
    return True


index = 1
for x in range(245690, 245756 + 1):
    if simple(x):
        print(index, x)
    index += 1
