from math import sqrt


def f(x):
    d = []
    for i in range(2, int(sqrt(x)) + 1):
        if x % i == 0:
            pair = x // i
            d.append(pair)
            d.append(i)
    if len(d) >= 2:
        d.sort()
        m = d[-1] + d[-2]
        if 0 < m < 10000:
            return m


for x in range(10_000_000, 100_000_000):
    if f(x):
        print(f(x))