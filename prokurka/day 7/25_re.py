import re


def check(s):
    for i in range(len(s) - 1):
        if s[i] >= s[i + 1]:
            return False
    return True


for x in range(1_000_000_000):
    if re.match("1\d*5\d*9", str(x)) and x % 21 == 0 and check(str(x)):
        print(x, x // 21)