def f(x):
    d = []
    for i in range(2, x + 1):
        if x % i == 0 and i % 2 == 0:
            d.append(i)
    if len(d) == 4:
        return d


for x in range(110203, 110245 + 1):
    if f(x):
        print(f(x))
