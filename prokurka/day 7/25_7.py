from math import sqrt


def f(x):
    d = []
    for i in range(1, int(sqrt(x)) + 1):
        if x % i == 0:
            pair = x // i
            dif = abs(i - pair)
            d.append(dif)
    count = 0
    for x in d:
        if x <= 100:
            count += 1
    if count >= 3:
        return True
    else:
        return False


for x in range(1_000_000, 2_000_001):
    if f(x):
        print(x)