from math import sqrt


def f(x):
    d = []
    for i in range(2, int(sqrt(x)) + 1):
        if x % i == 0:
            d.append(i)
            d.append(x // i)
    if len(d) == 2:
        return d


for x in range(174457, 174505 + 1):
    if f(x):
        print(f(x))
