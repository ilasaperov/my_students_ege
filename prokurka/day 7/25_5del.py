from math import sqrt


def simple(x):
    for i in range(2, x):
        if x % i == 0:
            return False
    return True


def f(x):
    if sqrt(x) == int(sqrt(x)):
        root = int(sqrt(x))
        if sqrt(root) == int(sqrt(root)):
            p = int(sqrt(root))
            if simple(p):
                return True
    return False


for x in range(35_000_000, 40_000_000 + 1):
    if f(x):
        print(x)