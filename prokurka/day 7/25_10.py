def f(x):
    for i in range(2, x):
        if x % i == 0 and i != 8 and i % 10 == 8:
            return i


for x in range(500001, 100000000):
    if f(x):
        print(x, f(x))