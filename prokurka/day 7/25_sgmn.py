def f(x):
    d = []
    m = 1
    for i in range(2, x + 1):
        if x % i == 0 and len(d) < 5:
            d.append(i)
            if len(d) == 5:
                for k in d:
                    m *= k
    if m == 1:
        return 0
    else:
        return m


for x in range(200_000_001, 1_000_000_000):
    if 0 < f(x) < x:
        print(f(x))