from math import sqrt


def simple(x):
    for i in range(2, x):
        if x % i == 0:
            return False
    return True


def f(x):
    if x % 2 == 0:
        x = x // 2
        if sqrt(x) == int(sqrt(x)):
            root = int(sqrt(x))
            if simple(root):
                return True
    return False


for x in range(101_000_000, 102_000_000 + 1):
    if f(x):
        print(x)