f = open("24 (33).txt")
a = f.read().replace("AB", "*").replace("CAC", "#")

count = 0
max_count = -1

for x in a:
    if x == "*":
        count += 2
        if count > max_count:
            max_count = count
    elif x == "#":
        count += 3
        if count > max_count:
            max_count = count
    else:
        count = 0

print(max_count)