f = open()
a = f.read().splitlines()[1:]

sum = 0
min_dif = 100000000000

for x in a:
    k, l = map(int, x.split())
    sum += max(k, l)
    dif = abs(k - l)
    if dif < min_dif and dif % 3 != 0:
        min_dif = dif

if sum % 3 != 0:
    print(sum)
else:
    print(sum - min_dif)
