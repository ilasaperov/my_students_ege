def f(x):
    s = bin(x)[3:]
    s = str(int(s))
    if s.count("1") % 2 == 0:
        s = "10" + s
    else:
        s = "1" + s + "0"
    return int(s, 2)

d = []
for x in range(2, 1000000):
    if f(x) < 450:
        d.append(f(x))

print(max(d))