f = open("26 (29).txt")

a = f.read().splitlines()
a = a[1:]
a = list(map(int, a))
a.sort()
half = a[2499]
quarter = a[3750]

count = 0
min_sr = 1000000000

for i in range(len(a) - 1):
    for j in range(i + 1, len(a)):
        sum = a[i] + a[j]
        if sum % 2 == 0:
            sr = sum // 2
            if half < sr < quarter:
                count += 1
                if sr < min_sr:
                    min_sr = sr

print(count, min_sr)
