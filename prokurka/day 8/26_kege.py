f = open("26 (33).txt")
a = f.read().splitlines()[1:]

dolg = {i: [] for i in range(-1800, 1801)}

for x in a:
    lat, long = map(int, x.split())
    dolg[long].append(int(lat / 10))


max_dolg = 0
max_count = 0

for id in dolg:
    if len(dolg[id]) > max_count:
        max_count = len(dolg[id])
        max_dolg = id

print(max_dolg, len(set(dolg[max_dolg])))

print(dolg[max_dolg])