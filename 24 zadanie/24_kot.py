f = open("24-j5.txt")
a = f.read()

count = 0
max_count = 0

for i in range(0, len(a) - 2, 3):
    if a[i] == "K" and a[i + 1] == "O" and a[i + 2] == "T":
        count += 1
        if count > max_count:
            max_count = count
    else:
        count = 0

print(max_count)
