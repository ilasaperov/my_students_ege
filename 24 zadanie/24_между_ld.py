'''
Определите символ, который чаще всего встречается в файле между двумя одинаковыми символами.
AABGEWFEF
'''
f = open("")

a = f.read()

count = []

for i in range(1000):
    count.append(0)


for i in range(1, len(a) - 1):
    if a[i - 1] == a[i + 1]:
        index = ord(a[i])
        a[index] += 1

max = 0
max_index = 0

for i in range(len(count)):
    if count[i] > max:
        max = count[i]
        max_index = i

print(chr(max_index))
