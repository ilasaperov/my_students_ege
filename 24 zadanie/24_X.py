'''
максимальное кол-во X подряд
'''

f = open("24_demo (17).txt")

a = f.read()

max_count = 0
count = 0

for i in range(0, len(a)):
    if a[i] == "X":
        count += 1
        if count > max_count:
            max_count = count
    else:
        count = 0

print(max_count)