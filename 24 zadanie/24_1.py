f = open("24_demo.txt")

a = f.read()

count = 0
count_max = 0

for x in a:
    if x == "X":
        count += 1
        if count > count_max:
            count_max = count
    else:
        count = 0

print(count_max)
