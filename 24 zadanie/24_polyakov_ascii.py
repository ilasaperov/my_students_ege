a = "ZXCABC"

count = 0
count_max = 0
index = 0
index_min = 1000000000
prev = 0

for i in range(len(a)):
    if prev == 0 or prev < ord(a[i]):
        count = 1
        index = i + 1
        prev = ord(a[i])
        if count > count_max:
            count_max = count
    elif prev > ord(a[i]):
        count += 1
        prev = ord(a[i])
        if count > count_max:
            count_max = count
            index_min = index

print(count_max, index_min)
