'''
Определите символ, который чаще всего встречается в файле после двух одинаковых символов.
'''
f = open("")

a = f.read()

count = []

for i in range(26):
    count.append(0)


for i in range(len(a) - 2):
    if a[i] == a[i + 1]:
        index = ord(a[i + 2]) - ord("A")
        a[index] += 1

max = 0
max_index = 0

for i in range(len(count)):
    if count[i] > max:
        max = count[i]
        max_index = i

print(chr(max_index + ord("A")))
