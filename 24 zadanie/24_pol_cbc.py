f = open("24-179.txt")

a = f.read()

count_c = 0
count_d = 0
count_e = 0

for i in range(len(a) - 4):
    if a[i] == "C" and a[i + 1] == "B" and a[i + 2] == "C" and a[i + 3] == "B" and a[i + 4] == "C":
        count_c += 1
    if a[i] == "C" and a[i + 1] == "B" and a[i + 2] == "D" and a[i + 3] == "B" and a[i + 4] == "C":
        count_d += 1
    if a[i] == "C" and a[i + 1] == "B" and a[i + 2] == "E" and a[i + 3] == "B" and a[i + 4] == "C":
        count_e += 1

print(count_d, count_e, count_c)