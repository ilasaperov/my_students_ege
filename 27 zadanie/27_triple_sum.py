"""
https://inf-ege.sdamgia.ru/problem?id=35916
"""


f = open("27-B (6).txt")

a = f.read().splitlines()

a = a[1:]

a = list(map(int, a))

a.sort()

m1 = [100000000, 100000000, 100000000]
m0 = [100000000, 100000000, 100000000]
m2 = 1000000000

for x in a:
    if x % 3 == 0:
        if x < m0[0]:
            m0[2] = m0[1]
            m0[1] = m0[0]
            m0[0] = x
        elif x < m0[1]:
            m0[2] = m0[1]
            m0[1] = x
        elif x < m0[2]:
            m0[2] = x
    elif x % 3 == 1:
        if x < m1[0]:
            m1[2] = m1[1]
            m1[1] = m1[0]
            m1[0] = x
        elif x < m1[1]:
            m1[2] = m1[1]
            m1[1] = x
        elif x < m1[2]:
            m1[2] = x
    else:
        if x < m2:
            m2 = x

print(min(sum(m0), sum(m1), m2 + m1[0] + m0[0]))
