"""https://inf-ege.sdamgia.ru/problem?id=27991"""
f = open("27991_B.txt")

a = f.read().splitlines()

a = a[1:]

a = map(int, a)

max_0 = -1
max_1 = -1

max_17_0 = -1 # 17 + odd + max

max_17_1 = -1 # 17 + even +  max


for x in a:
    if x % 17 == 0 and x % 2 == 0 and x > max_17_0:
        max_17_0 = x
    elif x % 2 == 0 and x > max_0:
        max_0 = x
    elif x % 17 == 0 and x % 2 != 0 and x > max_17_1:
        max_17_1 = x
    elif x % 2 != 0 and x > max_1:
        max_1 = x


sum_1 = max_1 + max_17_1
sum_2 = max_0 + max_17_0

print(sum_1, sum_2)
