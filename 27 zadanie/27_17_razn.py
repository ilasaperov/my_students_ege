"""https://inf-ege.sdamgia.ru/problem?id=27991"""
f = open("27991_B.txt")

a = f.read().splitlines()

a = a[1:]

a = map(int, a)

max_0 = -1
max_1 = -1

max_17_0_1 = -1 # 17 + odd + max
max_17_0_0 = -1 # 17 + odd + предпоследнее

max_17_1_1 = -1 # 17 + even +  max
max_17_1_0 = -1 # 17 + even + предпоследнее


for x in a:
    if x % 17 == 0:
        if x % 2 == 0:
            if x > max_17_0_1:
                max_17_0_0 = max_17_0_1
                max_17_0_1 = x
            elif x > max_17_0_0:
                max_17_0_0 = x
        else:
            if x > max_17_1_1:
                max_17_1_0 = max_17_1_1
                max_17_1_1 = x
            elif x > max_17_1_0:
                max_17_1_0 = x
    else:
        if x % 2 == 0 and x > max_0:
            max_0 = x
        elif x % 2 == 1 and x > max_1:
            max_1 = x

sum_1 = max_17_0_0 + max_17_0_1
sum_2 = max_17_1_0 + max_17_1_1
sum_3 = max_1 + max_17_1_1
sum_4 = max_0 + max_17_0_1

print(sum_1, sum_2, sum_3, sum_4)