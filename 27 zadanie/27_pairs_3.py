f = open("27-A (1).txt")
a = f.read().splitlines()
a = a[1:]

count_0 = 0
count_1 = 0
max_1 = 0
s = 0
for i in range(len(a)):
    x, y = map(int, a[i].split())
    if x < y:
        if x % 2 == 0:
            count_0 += 1
        else:
            count_1 += 1
        s += x
    else:
        if y % 2 == 0:
            count_0 += 1
        else:
            count_1 += 1
        s += y
    if max_1 < abs(x - y) > 0 and abs(x - y) % 2 == 1:
        max_1 = abs(x - y)
if s % 2 == 0 and count_0> count_1:
    print(s)
if s % 2 == 0 and count_1 > count_0:
    print(s - max_1)
if s % 2 == 1 and count_0 > count_1:
    print(s - max_1)
if s % 2 == 1 and count_1 > count_0:
    print(s)







