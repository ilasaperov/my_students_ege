f = open("27-B_1 (1).txt")

a = f.read().splitlines()

a = a[1:]
s = 0
min_dif = 10000000000

for x in a:
    k, m = map(int, x.split())
    s += max(k, m)
    if abs(k - m) < min_dif and abs(k - m) % 5 != 0:
        min_dif = abs(k - m)

if s % 5 != 0:
    print(s)
else:
    print(s - min_dif)