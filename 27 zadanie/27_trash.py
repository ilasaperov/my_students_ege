f = open("trash.txt")
a = f.read().splitlines()
a = list(map(int, a))
n = a[0]
a = a[1:]

s = 0
left = right = 0

for i in range(n):
    if i <= n // 2:
        s += a[i] * i
    else:
        s += (n - i) * a[i]

min_sum = 100000000
left = sum(a[:len(a)//2])
right = sum(a) - left
sums = [0 for i in range(n)]
sums[0] = s

for i in range(1, n):
    sums[i] = sums[i - 1] - left + right
    sums[i] += a[i - 1] + a[i - 1]
    sums[i] = sums[i] - a[(n // 2 + i) % n - 1] - a[(n // 2 + i) % n - 1]
    left = left - a[i - 1] + a[(n // 2 + i) % n - 1]
    right = right - a[(n // 2 + i) % n - 1] + a[i - 1]

print(sums)
