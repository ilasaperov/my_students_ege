f = open("27_B.txt")

a = f.read().splitlines()

a = a[1:]
s = 0
min_d = 1000000
for x in a:
    y = list(map(int, x.split()))
    y.sort()
    s += y[2]
    if y[2] - y[0] > y[1] - y[0] and 0 < y[1] - y[0] < min_d and (y[1] - y[0]) % 109 != 0:
        min_d = y[1] - y[0]
    elif y[2] - y[0] < y[1] - y[0] and 0 < y[2] - y[0] < min_d and (y[2] - y[0]) % 109 != 0:
        min_d = y[2] - y[0]



if s % 109 == 0:
    print(s - min_d)
else:
    print(s)
