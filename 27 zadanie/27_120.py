f = open()
a = f.read().splitlines()[1:]
a = list(map(int, a))

m = [0 for i in range(120)]

max_sum = 0
first = 0
second = 0

for x in a:
    k = x % 120
    if m[(120 - k) % 120] > x and m[(120 - k) % 120] + x > max_sum:
        first = m[(120 - k) % 120]
        second = x
    if x > m[k]:
        m[k] = x
print(first, second)
