f = open()
a = f.read().splitlines()[1:]
a = list(map(int, a))

mins = [[0 for i in range(93)] for j in range(2)]

max_sum = 0
sum = 0

for x in a:
    sum += x
    ost_93 = sum % 93
    ost_2 = sum % 2
    if mins[ost_2][ost_93] == 0:
        mins[ost_2][ost_93] = sum
    else:
        if ost_2 == 0:
            if sum - mins[1][ost_93] > max_sum:
                max_sum = sum - mins[1][ost_93]
        else:
            if sum - mins[0][ost_93] > max_sum:
                max_sum = sum - mins[0][ost_93]

print(max_sum)