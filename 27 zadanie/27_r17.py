f = open("27991_B.txt")

a = f.read().splitlines()

a = a[1:]

a = map(int, a)

max_chet = []
max_nechet = []

max_17_chet = []
max_17_nechet = []

for x in a:
    if x % 17 == 0:
        if x % 2 == 0:
            max_17_chet.append(x)
        else:
            max_17_nechet.append(x)
    else:
        if x % 2 == 0:
            max_chet.append(x)
        elif x % 2 == 1:
            max_nechet.append(x)

max_nechet.sort(reverse=True)
max_chet.sort(reverse=True)
max_17_nechet.sort(reverse=True)
max_17_chet.sort(reverse=True)

print(max_nechet[:1])
print(max_chet[:1])
print(max_17_nechet[:10])
print(max_17_chet[:10])
