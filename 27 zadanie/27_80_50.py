f = open("28130_A.txt")
a = f.read().splitlines()[1:]
a = list(map(int, a))

more_50 = [0 for x in range(80)]
less_50 = [0 for x in range(80)]
count = 0

for x in a:
    if x > 50:
        more_50[x % 80] += 1
    else:
        less_50[x % 80] += 1

for i in range(1, 40):
    count += more_50[i] * more_50[80 - i]
    count += more_50[i] * less_50[80 - i]
    count += less_50[i] * more_50[80 - i]

count += more_50[0] * less_50[0]
count += more_50[40] * less_50[40]
count += more_50[0] * (more_50[0] - 1) // 2
count += more_50[40] * (more_50[40] - 1) // 2

print(count)