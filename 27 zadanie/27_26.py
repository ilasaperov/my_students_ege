f = open("27990_B.txt")

a = f.read().splitlines()

a = a[1:]

a = list(map(int, a))

max_26 = 0 # делится на 26
max_13 = 0 # делится на 13, но не делится на 2
max_2 = 0 # делится на 2, но не делится на 13
max = 0 # максимум среди всех остальных

for x in a:
    if x % 13 == 0 and x > max_13 and x % 2 != 0:
        max_13 = x
    if x % 2 == 0 and x > max_2 and x % 13 != 0:
        max_2 = x
    if x % 26 == 0 and x > max_26:
        max_26 = x
    elif x == max_26:
        max = x
    if x > max:
        max = x

if max_26 * max == 0:
    print("NO")
if max_2 * max_13 == 0:
    print("NO")

if max_26 * max > max_2 * max_13:
    print(max_26 * max)
else:
    print(max_2 * max_13)