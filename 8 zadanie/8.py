import itertools

s = "ГОЛ"
count = 0
for x in itertools.product(s, repeat=20):
    a = ''.join(x)
    if "ГГ" not in a and "ОО" not in a and "ЛЛ" not in a and a[0] != "Г" and a[19] != "Г" and "ОГО" not in a and "ЛГЛ" not in a:
        count += 1

print(count)