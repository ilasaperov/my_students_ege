def f(x, y):
    if y > x:
        z = x
        x = y
        y = z
    a = x
    b = y
    while b > 0:
        r = a % b
        a = b
        b = r
    if a == 11 and x == 66:
        return y


for x in reversed(range(1000)):
    for y in reversed(range(1000)):
        if f(x, y) is not None:
            print(f(x,y))
            break